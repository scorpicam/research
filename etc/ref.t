A) My first thought was to optimize operating systems with artificial
   intelligence.
B) My topic has changed to optimizing memory managers with artificial
   intelligence because a paper came out about optimizing operating
   systems with machine learning, filling my gap perfectly.
C) To what extent does the use of machine learning in Unix(tm)-like
   operating systems affect the efficiency of those systems?
D) To what extent does the use of machine learning in the memory
   manager of operating systems affect their efficiency?
  
   See above.
E) See above.
F) The use of machine learning in an operating system memory manager
   will negatively impact the efficiency of the system because the
   required resources will make the machine-learning-based system
   heavier than a traditional memory manager.
G) I am now writing a custom operating system instead of rewriting
   portions of Minix3.
H) No.
