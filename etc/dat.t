.TL
Progress Check: Data
.AU
Cameron Shortt
.IP 1.
To what extent does the use of machine learning in 
memory managers affect the efficiency of operating systems?
.IP 2.
The machine-learning-based memory managers will, on average,
be less efficient than the traditional memory manager, but
over time tend to be equally as efficient.
.IP 3/4.
The evidence definitely refutes the hypothesis, as
while the machine-learning-based memory managers
were less efficient, they did not improve over
the testing period.
.IP 5.
The results will be displayed as a line graph and
table in the appendix.
