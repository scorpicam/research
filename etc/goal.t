.\" did i reach my goals
.TL
Projectia
.AU
Cameron Shortt
.2C
.NH
Topic
.LP
Machine Learning in Operating System Memory Managers
.NH
Research Question
.LP
To what extent does the use of machine learning in operating system
memory management units impact the efficiency of UNIX(tm)-like
operating systems qua kernel fragmentation.
.NH
Method
.LP
Build machine learning unit and build it into the Minix kernel
build a hotpluggable memory managment generator, and run this with 'normal'
computer jobs.
.NH
Writing Goal
.LP
500 Words
.NH
Writing Progress
.LP
0 Words
.NH
Collection Goal
.LP
Program AI library
.NH
Collection Progress
.LP
Found AI Library
.NH
Assistance
.LP
No
