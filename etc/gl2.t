.TL
Skynet Controls Your Laptop
.AU
Cameron Shortt
.NH
Goals
.NH 2
Topic
.LP
Operating System Memory Management
.NH 2
Research Question
.LP
To what extent does the inclusion of
machine learning in operating system
memory management units impact the
efficiency of UNIX(TM)-like operating
systems?
.NH 2
Method
.LP
In brief, I will program a memory manager that
uses machine learning and compare its' resource
usage to that of the default Minix memory manager.
.NH 2
Roadblocks
.LP
Programming is hard.
.NH 2
Assistance
.LP
No
