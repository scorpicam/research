     AI/OS
Cameron  Shortt

      OVERVIEW
o Background........
o History...........
o Research Question.
o Thesis............
o Method............
o Results...........

       JARGON
OS : Operating System
MM : Memory Manager
DF : Defragmentor
ML : Machine Learning
GA : Genetic Algorithm
NN : Neural Network

    MORE JARGON
Z80   : Old Computer
CP/M  : OS for the Z80
Unics : Influential OS
DUO   : Unics-like OS
        with CP/M
        influences

D isk-oriented
U nics-like
O perating
  system

      BACKGROUND
o MM (1970) = MM (2023)
o AI (1970) < AI (2023)
o Improvements?

   LITERATURE REVIEW
o Zhang & Huang: AI + OS
o Bellard: Compressed NN
o Popov et al: Optimize 

RESEARCH QUESTION

THESIS

 METHOD
DUO + NN
DUO + GA
DUO + DF

@dat.png

           REFERENCES
[1] Fabrice Bellard.  Lossless
    Data Compression with
    Neural Networks. 2019
[2] Andrew Popov.  Genetic
    Algorithms for Optimization.
    Genetic Algorithms for
    Optimization - Application
    in Controller Design Problems,
    2003.
[3] Alex Sherstinsky.  Fundamentals
    of Recurrent Neural Networks
    (RNN) and Long Short-Term
    Memory (LSTM) Network.
    Physica D: Nonlinear Phenomena.
    2020.
[4] Ashish Vaswani, Noam
    Shazeer, Niki Parmer, Jacob
    Uszkoreit, Llion Jones,
    Aidan Gomez, Łukasz Kaiser,
    and Illia Polosukhin.
    Attention is All You Need.
    NIPS, 2017.
[5] Yiying Zhang and Yutong
    Huang.  "Learned": Operating
    Systems.  ACM SIGOPTS, 2019.

THANK YOU FOR
  LISTENING
