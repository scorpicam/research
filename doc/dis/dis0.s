.\" ildn - lind...
.NH
Results
.LP
This paper sought to improve the understanding of artificial
intelligence being used in time-sensitive applications, such
as the operating system memory manager tested here.  In the
environment tested, the machine-learning-based memory manager
failed spectacularly, failing to be more efficient or even as
efficient as a traditional hole-finding memory manager on the
Intel 8080, even in setups that negated the computation time
of the machine learning models, suggesting that machine
learning cannot be leveraged as effectively in embedded
systems.\[dg]
.FS
\[dg] See Figures 2.a & 2.b
.FE
Unfortunately, this result is limited to the Intel
8080 on a CP/M-compatible BIOS, and only considers a subset
of machine learning models, i.e. the genetic algorithm, RNN, and
Transformer.  The code quality also has a significant impact,
with almost no optimization done on the different memory
managers, as optimizing the code could have allowed the machine
learning models to perform better.  Most importantly, these
machine learning models were designed to exploit the capabilities
of modern GPUs, and since the Intel 8080 does not have a GPU,
the models were forced to run in an environment they were not
suited for.  Due to the similarities between the Intel 8080
and modern Intel/AMD processors (which covers the vast majority
of personal computers), it is conceivable that the models would
perform similarly on those machines, so the poor performance
on the CPU would make them poor candidates for inclusion in
a performance-centric operating system.  However, the lack
of testing on a GPU leaves a considerable gap that could
allow machine-learning-based memory managers to drastically
outperform the simpler traditional memory managers. 
