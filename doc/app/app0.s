.\" lind
.EQ
delim @@
.EN
.SH
Appendix
.DS L
.UH
Figure 1
.EQ
s sub n ~=~ W sub s s sub { n - 1 }
 ~+~ W sub r r sub { n - 1 }
 ~+~ W sub x x sub n + theta sub s
.EN
.EQ
r sub n ~=~ G( s sub n )
.EN
.DE
.DS L
.UH
Figure 2.a
.TS
tab(,), expand;
c s s s
c c c c
n n n n.
_
Execution Cycles of EATMEM (@ns@)
_
Control,Genetic,RNN,Transformer
.so app/eat.d
_
.TE
.DE

.DS L
.UH
Figure 2.b
.G1
ticks left off
cy = 0
bh = 0.3
th = 0.3
copy "app/eat-avg.d" thru X
  line from 0, cy to $1, cy
  line from $1, cy to $1, cy-bh
  line from 0, cy-bh to $1, cy-bh
  line from 0, cy to 0, cy-bh

  line from $3, cy-bh/2 to $4, cy-bh/2
  line from $3, cy-3*bh/4 to $3, cy-bh/4
  line from $4, cy-3*bh/4 to $4, cy-bh/4
  bullet at $1, cy-bh/2

  "  $2" ljust at 0, cy-bh-th

  cy = cy - 1
X
bars = -cy
frame invis ht bars/3 wid 3
label top "Average Execution Time of Eatmem (ns)"
label bot "Runtime (ns)"
.G2
.DE
