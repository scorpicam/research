.\" lind
.\" karpathy
Andrej Karpathy defined the neural network-based machine learning space,
popularizing the RNN, and publicising the research that allowed the it to
be improved so drastically by derivative models.  This blog post
``The Unreasonable Effectiveness of Recurrent Neural Networks'', while not
academic, accurately and concisely demonstrates the advantages of the RNN over
competing models, and includes a sample implementation that was instrumental
to the design of the RNN and Transformer included in this study.  Karpathy
defines in this blog the simplist possible RNN and uses it for simple text
generation over small datasets such as the works of Shakespeare or the Linux
kernel source to illuminate the degree to which the RNN can learn patterns in
the relatively microscopic datasets and generate nearly realistic text.
.[
karpathy
.]
Karpathy recognizes that the RNN is not the best model for text generation,
with the LSTM especially drastically outperforming the RNN, but states that the
RNN's simplicity offers time and speed advantages over heavier models like the
LSTM, which is infamous for its' high memory usage.
.[
karpathy
.]
.PP
As this blog presented the technically superior LSTM as a model that would not
perform well under tight memory conditions like those seen in operating system
memory managers, unlike other LSTM papers that simply avoid the issue, the LSTM
was omitted from this study, allowing for a tighter comparison between the
Genetic Algorithm, RNN, Transformer, and Defragmenting Memory Manager.
.PP
Karpathy has a long history of publishing reputable work in peer-reviewed
journals, has advised major businesses such as Cisco and Tesla on machine
learning in their technologies, and has founded small open source projects that
still influence some of most popular machine learning toolsets, such as PyTorch
and Tensorflow.  As Karpathy and this particular blog post had a significant
impact on the direction of machine learning (with a focus on a model used in
this study), the implementation shown could not ignored in the context of
lightweight machine learning models.
