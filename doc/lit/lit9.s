.\" gap
.NH 2
Gap
.LP
With the exception of Zhang and Huang, operating system research in the context
of machine learning has never been done.  As Zhang and Huang focused on the
.I
configuration
.R
of the various sections of the operating system while this study focused on
replacing the traditional memory manager with a machine learning model, these
two studies do not overlap.  Based on this research, the Genetic Algorithm
should perform best, followed by the Transformer and the RNN, with the
defragmenting memory manager performing the worst.
