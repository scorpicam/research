.\"[
.\"genetic algorithms for optimization andrey popov
.\"]
.PP
As the usage of machine learning in operating system memory manager units
would drastically increase the resource usage of those units, making the
memory manager an unintended bottleneck, the machine learning portion of
the memory management unit needs to be as light as possible, preventing
more effective (and trendy) models like the Transformer to effectively
operate.  To combat this, the models can be optimized, as is explored in
Popov's paper on optimizing genetic algorithms.  As genetic algorithms
offer a lighter weight approach to machine learning that can effectively be
implemented in the kernel, optimizations in this area can allow for the
performance impacts of machine learning to be negated.
.PP
Genetic Algorithms, while considerably more lightweight than standard
neural networks, are still far too heavy for inclusion in a resource
constrained environment like an operating system memory manager, mostly due
to the storage of the individual genes and the CPU time required to choose
which parent's chromosome gets chosen per the length of the entire gene.
This is compounded by the growth of the genes over time, allowing both the
growth of the model to solve harder and harder tasks and the growth of the
execution time of the model, preventing it from some the tasks in a meaningful
amount of time or, in the case of an operating system memory manager, at all.
Popov aimed to remedy this issue by using genetic algorithms to 
optimize other genetic algorithms, theoretically allowing the genetic algorithm
to perform in well incredibly resource-intensive environments (like that of an
operating system memory manager).  Unfortunately Popov found that a genetic algorithm is
far too complicated to be optimized by another genetic algorithm, but the
theory of optimization via genetic algorithms applies well to other algorithms,
most notably in that the traditional defragmentation algorithm used in
operating system memory managers can be optimized somewhat.
.[
popov
.]
While that
algorithm doesn't
.I
need
.R
optimization, as it already performs quite adequately, the optimization of
traditional algorithms by ML could prove to be a better use than the raw
insertion of ML into the kernel.
.PP
Popov has a history of publishing reputable works to peer-reviewed journals,
and the vast majority of his work is heavily cited.  As his paper focuses on
the optimization of genetic algorithms by genetic algorithms, not the usage
of machine learning (and thus genetic algorithms) in the operating system
memory manager, the question of whether genetic algorithms could improve the
efficiency of the memory manager is left unanswered.
