.\" literary analysis source 1 (?)
.NH
Literary Analysis
.LP
Operating systems have traditionally advanced at a much slower pace
than the hardware they run on, with the hardware commonly supporting
features that the operating system doesn't, as operating systems have
become relegated to being built entirely for use instead of research.
This is not altogether a bad thing, as operating systems have become
extremely mature and useful, but the underlying technologies have changed
little.  As Zhang and Huang state in their recent study, machine learning
has been used little in operating systems research, but can provide
drastic improvements in usability in several areas, e.g. configuration,
policies (the general algorithms used by the operating system), and mechanisms
(the actual implementation of the policies).
.[
zhang
.]
.PP
A ``learned'' operating system
as described in Zhang's paper could provide general performance increases
across the entire operating system, but the study only applies machine
learning to the memory management unit through static configuration,
while a more in-depth analysis would include both dynamic configuration
and a combination of both, where a statically configured machine learned
memory management unit would be trained before hand, a dynamically
configured machine learned memory management unit would be trained during
the normal running of the target machine, and the combination would be
self explanatory. 
.PP
Both Zhang and Huang have a history publishing peer-reviewed papers in
reputable journals, and as the study is primarily focused on the use of
ML for the
.I
configuration
.R
of various sections of the operating system (including the memory manager)
instead of replacing the memory manager with a custom neural network as in
this study, Zhang and Huang's study does not infringe on the solution of
this one.
