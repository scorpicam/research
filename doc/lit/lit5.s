.\" lind
.\" fundamentals of rnns and lstms
.PP
A Recurrent Neural Network (RNN) is a simple machine learning model defined
by a hidden state vector that ``remembers'' its' previous state, allowing the
model to perform significantly better than earlier machine learning models
like the Convolutional Neural Network (CNN) a fraction of the size and required
training time, that defined a new class of neural networks (recurrent units)
and revolutionized the machine learning field, directly leading to the first
text-generation models able to pass the Turing Test, such as the LSTM and
variants and the GPT family.
.[
RNN
.]
While the RNN is by far the most memory hungry of the models used in this paper
and is outclassed by newer models like the Transformer, its' incredibly simple
architecture allows it to perform reasonably with only a single layer and a
relatively small input space, which larger models like the Transformer fail
miserably at.
.PP
Sherstinsky mathematically defines the RNN,\[dg]
.FS
\[dg] See Figure 1
.FE
deriving it from the basic
differential equation, allowing for a rigorous analysis of the model in a more
useful form, rather than the recursive definition that is normally studied.
The proof by equivalance (derivation) shows that recursive definition can be
modeled as a differential equation, which formed the basis of other subsidiary
networks like the LSTM, which is also defined in the paper.
.[
RNN
.]
While the proofs
are by no means original, they consolidate the redefinition to a differential
equation to a single reference paper, allowing them to be more easily referenced
in more expansionary literature.
.PP
Sherstinsky has a long history of publishing reputable and heavily cited work
in peer-reviewed journals.  As the RNN can not be reasonably omitted from a
machine learning study involving text generation, it is one of the three models
used here.

