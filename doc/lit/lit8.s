.\" lind
.\" gradient-based learning algorithms
.\" rnn-2
.PP
The machine learning models used in this study can be divided into two
categories: genetic and gradient.  The gradient-based algorithms are built on
matrix transformations (unsurprisingly), allowing for nonlinear relationships to
be found in the dataset, allowing for significantly improved sequence generation
output, i.e., more realistic text, or in this case, more efficient memory
management.  In ``Gradient-Based Learning Algorithms for Recurrent Networks and
Their Computational Complexity'', Williams discusses multiple variations of the
traditional RNN, each improving the performance and allowing for more complex
and slightly more efficient or accurate models, while considering the tradeoffs
of the added complexity.  The traditional RNN is already the simplest member of
the family of derivations, being a single cell calculation versus the multiple
more computationally heavy calculations required for more accurate models such
as the LSTM.
.[
gradient
.]
While significantly increasing the size, unrolling the model allows for
an unexpected increase in efficiency analagous to that found in loop unrolling
in assembly programming.  Williams discusses the usage of this is great detail,
with a particular emphasis on how this affects backpropagation, finding that
backpropagation can still be achieved, resulting in ``real-time backpropagation
through time.''
.[
gradient
.]
By finding that backpropagation could be made real-time, the efficiency of the
RNN was significantly increased, allowing for the improved algorithm to
calculate more nodes of the network in the same time as the traditional RNN,
allowing for more accurate patterns, more realistic text generation, and more
efficient memory management (in this case).
.PP
Williams has a history of publishing reputable work in peer-reviewed journals,
and as this study defines several optimizations for the traditional RNN that
could allow it to perform better (more accurate results and more efficient
memory management), it could allow for the RNN to compete with the defragmenting
memory manager.
