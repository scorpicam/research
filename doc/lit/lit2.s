.\" lind
.\" lossless data compression with neural networks - bellard
.PP
A major problem with the application of neural networks in resource-constrained
environments such as that of the operating system memory manager is, somewhat
obviously, resource usage.  Modern neural networks, especially RNNs or Transformers,
can quite easily consume vast amounts of CPU or VRAM to function properly, which
has in the past made most larger (and thus more effective) models unsuitable for
such environments, and thus neural networks have seen limited usage in critical
operating systems areas.  Bellard's case study on the optimization of LSTM and
Transformer models for his experimental data compression algorithm helps alleviate
this by allowing the larger models to run in the constrained data-compression
environment, thus making them theoretically competitive with traditional compression
algorithms.
.[
zip
.]
As DUO makes limited use of data compression in the memory manager
and heavy use of large language models in most of the variations, Bellard's case
study allows for the inclusion of large language models where the models can
compete with the smaller ML algorithms and traditional memory management algorithms.
.PP
Bellard outlined in his paper several optimizations for both the LSTM and
Transformer models, most notably extensive caching, and an updated arithmetic
encoder, and redefined the models in forms that are significantly lighter on
CPU.  Although these changes reduce the overall efficiency of the model for high
quality results, the operating system memory manager will be impacted more by the
resource usage of the models than by a slightly longer execution time.
.[
zip
.]
As this
performed better than traditional lossless compression models in the paper, and
as compression is generally more resource intensive and demanding of the model,
the optimized models could perform well in the resource-constrained environment
of an operating system memory manager.  Bellard has a history publishing heavily
cited papers, and has started multiple major open source projects that revolve
around heavy optimization, such as the QEmu emulator this study used, the TinyCC
C language compiler, which currently outperforms all other major C language
compilers, and the PCjs emulator, which is capable of running modern operating
systems like Windows 11 in a web browser at impressive speeds.  As Bellard's study
focuses on using ML for compression instead of using ML for memory optimization
in this paper, these studies only overlap in the usage of optimized neural networks.
