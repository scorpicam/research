.\" lind
.\" vaswani et. al attention is all you need, vaswani
.PP
In Vaswani et. al's now famous paper "Attention Is All You Need", a new class
of RNN-based models is defined where half of the RNN (memory) is omitted to
create a model that converges on comparable acceptable solutions much faster
than traditional RNNs with much smaller networks.
.PP
RNN class neural network neurons have two major components: attention and
memory.  Memory holds a history of everything that has passed through the
neuron, allowing the neuron to make decisions based on what has come before,
and attention holds points in the memory that the neuron marks as
important (sometimes as actual pointers to points in the memory vector).
Vaswani et. al's study omits the entire memory portion of the neuron while
only making minor changes to the neuron structure.  This new model, dubbed the
Transformer, converges of solutions significantly faster than all other types
of machine learning models.
.[
vaswani
.]
Unfortunately, despite the smaller network sizes, the Transformer requires
more computationally heavy matrix multiplications, making it slower by default
than stock RNNs.  Despite this, the Transformer has proven to show significantly
better results than other neural networks, and has formed the basis for commonly
know machine learning models such as GPT-3, ChatGPT, Midjourney, and Stable
Diffusion, and thus can not be omitted from any study researching the impact
of general machine learning.
.PP
Vaswani et. al have a long history of publishing reputable work in peer
reviewed journals, and are responsible for major advances in natural language
processing via machine learning.  As the Transformer can not be reasonably
omitted from this study due to its' influence on machine learning research,
it is included alongside the RNN despite the size and relative inefficiency 
of the models.
