.\" lind
.\" bellard lossless data compression with transformer
.PP
Bellard's purpose for optimizing the Transformer as discussed
was to create a novel compression algorithm
via artificial intelligence.  As compression algorithms have
to run with a small memory footprint but smaller models did not
perform adaquetly compared to traditional compression algorithms
(the artificial intelligence-based algorithms were significantly
larger and slower).
.[
nncp
.]
To combat this, Bellard compressed and
optimized the Transformer machine learning algorithm, allowing
the novel algorithm to compress the test data to much smaller
sizes than the traditional algorithms.
Large neural network models have
.[
nncp
.]
traditionally been significantly too memory intensive to compete in constrained
environments like that of compression (in this case time constrained), but
Bellard's optimizations allowed the Transformer to perform significantly faster
than the stock Transformer with better results, allowing the novel compression
algorithm to better compress the input data at acceptable speeds (not fast,
but faster than some traditional algorithms).
.[
nncp
.]
As this study requires the
machine learning models to be as fast and as efficient as possible, Bellard's
optimizations to the Transformer could allow it to perform similarly to
the traditional memory management algorithms.
