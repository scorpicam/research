.\" lind
.\" method
.\" 1. write os
.\" 2. plugin tons of different memory managers
.\" 3. record eatmem + sysstat
.NH
Method
.LP
To experimentally test the efficiency of a machine learning-based memory manager,
four parts are needed: an experiment, an operating system, different memory managers,
and a way to derive efficiency from the data.  Older studies that tested the
efficiency of traditional memory managers often experimentally found the efficiency
with a simple process that rarely changed between the papers: taking an
easily-extensible operating system, swapping the current memory manager (control) out
for the one being tested, and recording the time it takes for a memory intensive process
to complete.  This study employs a minor variation of this, with a custom
operating system for the Intel 8080 processor based loosely on CP/M and Unics,
the Disk-oriented Unics-based Operating System (DUO), designed to limit the number of
variables in the experiment.  As DUO only consists of the Unics filesystem (hardlink graph),
a memory manager, a round-robin process scheduler, and a simple command shell, it is
much easier to reprogram, unlike the standard operating system choices
which are ironically not designed for research and tend to be
unwieldy when a part needs to be edited.  Unfortunately, DUO is so lightweight the inbuilt
system statistics program would skew any data with its' own memory usage, so the runtime
of the choice program (eatmem) was instead measured with a separate debugger
watching the memory and clock of an emulator.  Efficiency was derived by finding
the median execution cycle, and comparing that to the other median cycle times.
To find the most efficient memory manager, each one was assembled into an independent
DUO kernel image, with each image was running on an independent instance of the Toledo
8080 emulator.  For each emulator instance, a diagnostic script combined with the
GNU Debugger would watch both the memory and clock variables, logging the clock
time whenever the memory usage dropped below the idle threshold (eatmem linearly allocates
memory until it is killed, thus dropping its' memory usage to zero), allowing for a consistent
and precise estimate for the execution time of eatmem.  DUO would at this point
restart eatmem and continue the process until the experiment is manually ended, thus
generating a satisfactory number of data points.  The median would be calculated
for each memory manager from this data set, and the comparison with the other medians
would yield the efficiency for each memory manager.
.PP
This process is applied to several different variations of the traditional defragmenting
memory manager, each being powered in part by either a genetic algorithm, a RNN or
a Transformer.  These models were chosen for their text-generation/modification abilities,
as they function by generating strings that the memory manager interprets as allocation
instructions.  Using interpreted strings allows the traditional static defragmentor
to perform somewhat equally with the dynamic neural networks, as static nature of
the defragmentor removes a significant amount of overhead that cannot be removed
from the dynamic machine learning memory managers.  The interpreter adds back the
overhead without unduly weighing down the machine learning memory managers.  All of
the models are implemented as part of the memory manager, dynamically generating
strings to be interpreted, so the median of the data set is used for efficiency instead
of the average, which is normally used for efficiency measurements, so the initial
extremely poor behavior of the machine learning memory managers will not skew their
efficiency scores.
