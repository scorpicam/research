.\" lind
.SH
Abstract
.LP
As machine learning has not made its' way into most
mainline operating systems, this study tested whether
the inclusion of machine learning models in the
operating system memory manager by swapping out several
different memory managers each with a different model
included through a simple CP/M based operating system
(DUO).  This study found that there was a distinct
drop in the performance of the memory manager as the
models grew in size, with the Transformer performing
worst and the control performing best, suggesting a
negative correlation between the size of the model and
the model's efficiency.
