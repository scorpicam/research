.\" intro
.NH
Introduction
.LP
Across almost all operating systems, the algorithms used for memory management
have not changed since the late 1990s.  As this is due to the algorithms
becoming quite efficient for the systems they were designed for qua
efficiency qua virtual memory and defragmentation, research in the area has
considerably decreased, with time being refocused to more popular problems,
such as machine learning.  This study aims to combine these two fields (machine
learning and operating systems research) to improve the efficiency of operating
system memory management for systems that run similar sets of software 
repeatedly.  The tried-and-true algorithms are as efficient as possible for
defragmentation, but they still waste time solving the fragmentation that
occurs.  Machine learning can be applied to prevent fragmentation in the
first place.
.PP
This study does not aim to improve on defragmentation or virtual memory
techniques, as these have been mathematically proven to be as efficient as
possible; rather, prevent fragmentation from occurring in the first place by
predicting how processes allocate memory allocating the memory accordingly.
Previously, machine learning techniques were too memory intensive to be useful
in the context of operating system memory management, as everything running at
that level has been optimized to the point of maximum perfection allowed by
the traditional algorithms, but
intensive research into optimizing the memory management of neural networks
has allowed for its inclusion into lower level system functions.  This study
will be testing how several different types of neural networks, including the
Recurrent Neural Network (RNN), Long
Short-Term Memory Network (LSTM), Transformer, as well as an
evolutionary model: the genetic algorithm.  These networks will be
applied across different contexts, i.e. asynchronously and concurrently, so as
to test whether the overhead of a machine learning unit will reasonably
impact modern operating system efficiency.  Fragmentation is a major issue
in operating system design, as it can measurably impact how much work the
computer can accomplish over a unit of time, to the point of impacting end
users.  This study aims to help alleviate this issue.

