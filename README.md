# AI-Based Memory Manager

This is the source for my AP Capstone Research class paper
about using machine learning to augment operating system
memory managers.

- bin: build scripts
- doc: source for paper
- etc: prewriting files
- pres: presentation source
